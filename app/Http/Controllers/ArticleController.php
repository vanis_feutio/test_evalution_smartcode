<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;

class ArticleController extends Controller
{

    public function getListArticle()
    {
        $url = config('app.newApi_base_url') . "everything?q=apple&from=" . date("Y-m-d") .  "&to=" . date("Y-m-d") . "&sortBy=publishedAt&apiKey=" . config('app.key_api_newApi');
        $response = Http::get($url);
        $data = $response->json();
        if ($data == null) {
            return 'Le serveur newApi.org ne répond pas; Statut: ' . $response->status();
        }
        if($response->status() == 200){
            return view('articles.getAll',[
                'data' => $data,
            ]);
        }
        else {
            return "API Error";
        }
    }

    public function getArticle($date)
    {
        $url = config('app.newApi_base_url') . "everything?q=apple&from=" . date("Y-m-d") .  "&to=" . date("Y-m-d") . "&sortBy=publishedAt&apiKey=" . config('app.key_api_newApi');
        $response = Http::get($url);
        $data = $response->json();
        if ($data == null) {
            return 'Le serveur newApi.org ne répond pas; Statut: ' . $response->status();
        }
        if($response->status() == 200){
            foreach ($data['articles'] as $article) {

                if ($article["publishedAt"]== $date) {
                    return view('articles.getByDate',[
                        'article' => $article,
                    ]);
                }
                return "Article not found";
            }
        }
        else {
            return "API Error";
        }
    }

    public function souscrire()
    {
        return "ok";
    }
}
