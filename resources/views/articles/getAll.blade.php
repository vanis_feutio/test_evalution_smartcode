@extends('layout.app')
@section('content')
    <div class="contener-fluid">
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Souscription</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="/souscrire" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">souscrire</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
        <div class="row mb-2 mt-4">
            @foreach ($data["articles"] as $article)
                <div class="col-md-6">
                    <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-primary">Titre</strong>
                        <h3 class="mb-0">{{$article['title']}}</h3>
                        <div class="mb-1 text-muted">Auteur : {{$article['author']}}</div>
                        <p class="card-text mb-auto">{{$article['description']}}</p>
                        <p class="card-text mb-auto">Date publucation: {{$article['publishedAt']}}</p>
                        <a href="/article/{{$article['publishedAt']}}" class="stretched-link">Voir plus de details</a>
                        <a href="/souscrire" data-bs-toggle="modal" data-bs-target="#exampleModal" class="stretched-link">Souscrire ?</a>
                        </div>
                        <div class="col-auto d-none d-lg-block">
                            <img class="bd-placeholder-img" width="200" height="250" src="{{$article['urlToImage']}}" alt="Image de l'article">
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
</body>
</html>
