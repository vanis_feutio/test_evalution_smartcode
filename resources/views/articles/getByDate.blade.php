@extends('layout.app')
@section('content')
    <div class="contener-fluid">
        <div class="col-md-12">
            <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                <div class="col p-4 d-flex flex-column position-static">
                <strong class="d-inline-block mb-2 text-primary">Titre</strong>
                <h3 class="mb-0">{{$article['title']}}</h3>
                <div class="mb-1 text-muted">Auteur : {{$article['author']}}</div>
                <p class="card-text mb-auto">{{$article['description']}}</p>
                <p class="card-text mb-auto">{{$article['content']}}</p>
                <p class="card-text mb-auto">Date publucation: {{$article['publishedAt']}}</p>
                <a href="/" class="stretched-link">Retour</a>
                </div>
                <div class="col-auto d-none d-lg-block">
                    <img class="bd-placeholder-img" width="200" height="250" src="{{$article['urlToImage']}}" alt="Image de l'article">
                </div>
            </div>
        </div>
    </div>
@endsection

